//Write a program to find the sum of n different numbers using 4 functions

#include<stdio.h>
int main()
{
  int n, sum=0, c, value;
  printf("Enter the number of integers to add: ");
  scanf("%d",&n);
  
  printf("Enter %d integers", n);
  for(c=1;c<=n;c++)
  {
    scanf("%d",&value);
    sum += value;
  }
  printf("sum of entered numbers=%d",sum);
  return 0;
}
