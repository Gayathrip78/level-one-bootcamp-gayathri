//WAP to find the sum of two fractions

#include<stdio.h>
struct fraction
{
int nr;
int dr;
};
typedef struct fraction Fraction;
Fraction input(char a[])
{
Fraction temp;
printf("enter the value of %s: ",a);
scanf("%d%d",&temp.nr,&temp.dr);
return temp;
}
int find_gcd(int a, int b)
{
int temp;
while(a!=0)
{
temp=a;
a=b%a;
b=temp;
}
return b;
}
Fraction simplify(Fraction sum)
{
int gcd=find_gcd(sum.nr,sum.dr);
sum.nr=sum.nr/gcd;
sum.dr=sum.dr/gcd;
return sum;
}
Fraction compute_sum(Fraction num1,Fraction num2)
{
Fraction sum;
sum.nr=(num1.nr*num2.dr)+(num2.nr*num1.dr);
sum.dr=(num1.dr*num2.dr);
sum=simplify(sum);
return sum;
}

void display_sum(Fraction num1,Fraction num2,Fraction sum)
{
printf("the sum of %d/%d and %d/%d is %d/%d",num1.nr,num1.dr,num2.nr,num2.dr,sum.nr,sum.dr);
}

int main()
{
Fraction num1=input("a/b");
Fraction num2=input("c/d");
Fraction sum=compute_sum(num1,num2);
display_sum(num1,num2,sum);
return 0;
}

